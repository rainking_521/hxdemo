package com.rulin.huanxin_demo.utlis.im.listener;

import android.util.Log;

import com.hyphenate.EMMessageListener;
import com.hyphenate.chat.EMMessage;
import com.hyphenate.util.EasyUtils;
import com.rulin.huanxin_demo.base.BaseApp;
import com.rulin.huanxin_demo.utlis.NotifyUtils;
import com.rulin.huanxin_demo.utlis.StringUtil;
import com.rulin.huanxin_demo.utlis.event.ComNotifyConfig;
import com.rulin.huanxin_demo.utlis.event.ComNotifyEventBean;
import com.rulin.huanxin_demo.utlis.event.EventBusHelper;
import com.rulin.huanxin_demo.utlis.event.HxMessageEventBean;


import java.util.HashSet;
import java.util.List;

/**
 * Created by LWK
 * TODO 环信消息监听
 * 2016/9/20
 */
public class HxMessageListener implements EMMessageListener {
    private static final String TAG = "HxMessageListener";
    //存放无需铃声提醒的会话id
    private HashSet<String> conversationSet = new HashSet<>();

    //添加无需铃声提醒的会话id
    public void addConId(String conId) {
        if (StringUtil.isNotEmpty(conId))
            conversationSet.add(conId);
    }

    //移除无需铃声提醒的会话id
    public void removeConId(String conId) {
        if (StringUtil.isNotEmpty(conId))
            conversationSet.remove(conId);
    }

    //收到消息
    @Override
    public void onMessageReceived(List<EMMessage> list) {

        Log.i(TAG, "HxMessageListener onMessageReceived : " + list);
        //应用处于后台就发送通知栏提醒
        if (!EasyUtils.isAppRunningForeground(BaseApp.getInstance())) {
            NotifyUtils.getInstance().sendMessageNotifivation(list);
        } else {
            //应用处于前台就发送铃声、震动通知
            //震动通知不受限制
            //铃声通知需要检查单条消息所属会话是否无需提醒,多条消息不受限制
            NotifyUtils.getInstance().vibratorNotify();
            if (list.size() == 1 && !conversationSet.contains(list.get(0).getFrom()))
                NotifyUtils.getInstance().ringtongNotify();
            else if (list.size() > 1)
                NotifyUtils.getInstance().ringtongNotify();
        }
        //通知刷新未读消息数
        EventBusHelper.getInstance().post(new ComNotifyEventBean(ComNotifyConfig.REFRESH_UNREAD_MSG));
        //通知收到新消息
        EventBusHelper.getInstance().post(new HxMessageEventBean(HxMessageEventBean.NEW_MESSAGE_RECEIVED, list));
    }

    //收到透传消息
    @Override
    public void onCmdMessageReceived(List<EMMessage> list) {
        Log.i(TAG, "HxMessageListener onCmdMessageReceived : " + list);
    }

    //收到已读回执
    @Override
    public void onMessageReadAckReceived(List<EMMessage> list) {
        Log.i(TAG, "HxMessageListener onMessageReadAckReceived : " + list);
    }

    //收到已送达回执
    @Override
    public void onMessageDeliveryAckReceived(List<EMMessage> list) {

        Log.i(TAG, "HxMessageListener onMessageDeliveryAckReceived : " + list);
    }

    //消息状态变动
    @Override
    public void onMessageChanged(EMMessage emMessage, Object o) {

        Log.i(TAG, "HxMessageListener onMessageChanged : message = " + emMessage);
    }
}
