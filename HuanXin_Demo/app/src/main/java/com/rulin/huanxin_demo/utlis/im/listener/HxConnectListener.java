package com.rulin.huanxin_demo.utlis.im.listener;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;

import com.hyphenate.EMConnectionListener;
import com.hyphenate.EMError;
import com.rulin.huanxin_demo.LoginActivity;
import com.rulin.huanxin_demo.R;
import com.rulin.huanxin_demo.base.BaseApp;
import com.rulin.huanxin_demo.utlis.ActivityManagerUtil;
import com.rulin.huanxin_demo.utlis.FCError;
import com.rulin.huanxin_demo.utlis.event.ConnectEventBean;
import com.rulin.huanxin_demo.utlis.event.EventBusHelper;
import com.rulin.huanxin_demo.utlis.im.helper.HxSdkHelper;

/**
 * Created by LWK
 * TODO 环信连接监听
 * 2016/8/8
 */
public class HxConnectListener implements EMConnectionListener {
    private static final String TAG = "HxConnectListener";
    private Handler mHandler;

    public HxConnectListener() {
        mHandler = new Handler(Looper.getMainLooper());
    }

    @Override
    public void onConnected() {

        Log.d(TAG, "HxConnectListener onConnected");
        //发送Event
        ConnectEventBean eventBean = new ConnectEventBean(true, -1);
        EventBusHelper.getInstance().post(eventBean);
//        //同步环信好友
//        UserDao.getInstance().updateUserFromHxServer();
    }

    @Override
    public void onDisconnected(int i) {

        Log.i(TAG, "HxConnectListener--->onDisconnected：code=" + i);
        switch (i) {
            //账号被移除
            case EMError.USER_REMOVED:
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        userBeRemoved();
                    }
                });
                break;
            case EMError.USER_NOT_FOUND:
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        userBeRemoved();
                    }
                });
                break;
            //账号在其他地方登录
            case EMError.USER_LOGIN_ANOTHER_DEVICE:
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        userReLogined();
                    }
                });
                break;
            default:
                ConnectEventBean eventBean = new ConnectEventBean(false);
                eventBean.setErrorMsgId(FCError.getErrorMsgIdFromCode(i));
                EventBusHelper.getInstance().post(eventBean);
                break;
        }
    }

    //执行用户账号被移除后的操作
    private void userBeRemoved() {
        HxSdkHelper.getInstance().logout(null);
        Activity activity = ActivityManagerUtil.getInstance().getLastActivity();
        if (activity instanceof LoginActivity)
            return;
        Toast.makeText(BaseApp.getInstance(), R.string.warning_user_be_removed, Toast.LENGTH_SHORT).show();
        activity.startActivity(new Intent(activity, LoginActivity.class));
        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                ActivityManagerUtil.getInstance().finishActivity(LoginActivity.class);
            }
        }, 500);
    }

    //执行用户在其他手机上登录后的操作
    public void userReLogined() {
        HxSdkHelper.getInstance().logout(null);
        final Activity activity = ActivityManagerUtil.getInstance().getLastActivity();
        new AlertDialog.Builder(activity)
                .setCancelable(false)
                .setTitle(R.string.dialog_user_relogin_title)
                .setMessage(R.string.dialog_user_relogin_content)
                .setPositiveButton(R.string.confrim, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (activity instanceof LoginActivity) {
                            dialog.dismiss();
                            return;
                        }
                        activity.startActivity(new Intent(activity, LoginActivity.class));

                        mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                ActivityManagerUtil.getInstance().finishActivity(LoginActivity.class);
                            }
                        }, 500);
                        dialog.dismiss();
                    }
                }).create().show();
    }
}
