package com.rulin.huanxin_demo.base;

import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;

import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMOptions;
import com.rulin.huanxin_demo.utlis.im.helper.HxSdkHelper;

import java.util.Iterator;
import java.util.List;

/**
 * Created by Raink on 2016/11/18.
 */

public class BaseApp extends Application {

    private static BaseApp mInstance;

    private static Context mContext;

    // 记录环信是否已经初始化
    private boolean isInit = false;

    public static BaseApp getInstance() {
        return mInstance;
    }


    public static Context getmContext() {
        return mContext;
    }

    public static void setmContext(Context mContext) {
        BaseApp.mContext = mContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;

        // 初始化环信SDK
        HxSdkHelper.getInstance().initSdkOptions(mContext);
    }
}
