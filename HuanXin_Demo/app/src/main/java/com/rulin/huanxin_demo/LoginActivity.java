package com.rulin.huanxin_demo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.hyphenate.EMCallBack;
import com.hyphenate.EMError;
import com.hyphenate.chat.EMClient;
import com.hyphenate.exceptions.HyphenateException;
import com.rulin.huanxin_demo.base.BaseActivity;
import com.rulin.huanxin_demo.storage.sp.SpSetting;
import com.rulin.huanxin_demo.utlis.FCCallBack;
import com.rulin.huanxin_demo.utlis.ToastUtils;
import com.rulin.huanxin_demo.utlis.im.helper.HxSdkHelper;

public class LoginActivity extends BaseActivity {

    // 弹出框
    private ProgressDialog mDialog;

    // username 输入框
    private EditText mUsernameEdit;
    // 密码输入框
    private EditText mPasswordEdit;

    // 注册按钮
    private Button mSignUpBtn;
    // 登录按钮
    private Button mSignInBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_login;
    }

    @Override
    protected void initView() {
        getToolbarLeftCenter(R.mipmap.ic_back, "返回", "登录");
        mUsernameEdit = (EditText) findViewById(R.id.ec_edit_username);
        mPasswordEdit = (EditText) findViewById(R.id.ec_edit_password);

        mSignUpBtn = (Button) findViewById(R.id.ec_btn_sign_up);
        mSignUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signUp();
            }
        });

        mSignInBtn = (Button) findViewById(R.id.ec_btn_sign_in);
        mSignInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();
            }
        });
    }


    /**
     * 注册方法
     */
    private void signUp() {
        // 注册是耗时过程，所以要显示一个dialog来提示下用户
        mDialog = new ProgressDialog(this);
        mDialog.setMessage("注册中，请稍后...");
        mDialog.show();


        String username = mUsernameEdit.getText().toString().trim();
        String password = mPasswordEdit.getText().toString().trim();


        HxSdkHelper.getInstance().regist(username, password, new FCCallBack() {
            @Override
            public void onFail(int status, int errorMsgResId) {
                mDialog.dismiss();
                ToastUtils.showLongMsg(LoginActivity.this, errorMsgResId);
            }

            @Override
            public void onSuccess(Object o) {

                if (!LoginActivity.this.isFinishing()) {
                    mDialog.dismiss();
                }
                Toast.makeText(LoginActivity.this, "注册成功", Toast.LENGTH_LONG).show();


            }
        });


    }

    /**
     * 登录方法
     */
    private void signIn() {
        mDialog = new ProgressDialog(this);
        mDialog.setMessage("正在登陆，请稍后...");
        mDialog.show();
        final String username = mUsernameEdit.getText().toString().trim();
        final String password = mPasswordEdit.getText().toString().trim();

        HxSdkHelper.getInstance().login(username, password, 2, new FCCallBack() {
            @Override
            public void onFail(int status, int errorMsgResId) {
                mDialog.dismiss();
                ToastUtils.showLongMsg(LoginActivity.this, errorMsgResId);
            }

            @Override
            public void onSuccess(Object o) {

                mDialog.dismiss();


                //存储最近登录账号的数据
                SpSetting.setLastLoginPhone(LoginActivity.this, username);
                SpSetting.setLastLoginPwd(LoginActivity.this, password);
                // 登录成功跳转界面
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);
                finish();


            }
        });
    }

//        EMClient.getInstance().login(username, password, new EMCallBack() {
//            /**
//             * 登陆成功的回调
//             */
//            @Override
//            public void onSuccess() {
//
//            }
//
//            /**
//             * 登陆错误的回调
//             * @param i
//             * @param s
//             */
//            @Override
//            public void onError(final int i, final String s) {
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        mDialog.dismiss();
//                        Log.d("lzan13", "登录失败 Error code:" + i + ", message:" + s);
//                        /**
//                         * 关于错误码可以参考官方api详细说明
//                         * http://www.easemob.com/apidoc/android/chat3.0/classcom_1_1hyphenate_1_1_e_m_error.html
//                         */
//                        switch (i) {
//                            // 网络异常 2
//                            case EMError.NETWORK_ERROR:
//                                Toast.makeText(LoginActivity.this, "网络错误 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
//                                break;
//                            // 无效的用户名 101
//                            case EMError.INVALID_USER_NAME:
//                                Toast.makeText(LoginActivity.this, "无效的用户名 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
//                                break;
//                            // 无效的密码 102
//                            case EMError.INVALID_PASSWORD:
//                                Toast.makeText(LoginActivity.this, "无效的密码 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
//                                break;
//                            // 用户认证失败，用户名或密码错误 202
//                            case EMError.USER_AUTHENTICATION_FAILED:
//                                Toast.makeText(LoginActivity.this, "用户认证失败，用户名或密码错误 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
//                                break;
//                            // 用户不存在 204
//                            case EMError.USER_NOT_FOUND:
//                                Toast.makeText(LoginActivity.this, "用户不存在 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
//                                break;
//                            // 无法访问到服务器 300
//                            case EMError.SERVER_NOT_REACHABLE:
//                                Toast.makeText(LoginActivity.this, "无法访问到服务器 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
//                                break;
//                            // 等待服务器响应超时 301
//                            case EMError.SERVER_TIMEOUT:
//                                Toast.makeText(LoginActivity.this, "等待服务器响应超时 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
//                                break;
//                            // 服务器繁忙 302
//                            case EMError.SERVER_BUSY:
//                                Toast.makeText(LoginActivity.this, "服务器繁忙 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
//                                break;
//                            // 未知 Server 异常 303 一般断网会出现这个错误
//                            case EMError.SERVER_UNKNOWN_ERROR:
//                                Toast.makeText(LoginActivity.this, "未知的服务器异常 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
//                                break;
//                            default:
//                                Toast.makeText(LoginActivity.this, "ml_sign_in_failed code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
//                                break;
//                        }
//                    }
//                });
//            }
//
//            @Override
//            public void onProgress(int i, String s) {
//
//            }
//        });
//    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initEvents() {

    }

    @Override
    protected void onClickLeft() {
        getFlinishActivity(LoginActivity.class);
        super.onClickLeft();
    }
}
