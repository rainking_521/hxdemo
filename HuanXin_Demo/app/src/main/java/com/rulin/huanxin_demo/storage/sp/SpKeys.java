package com.rulin.huanxin_demo.storage.sp;

/**
 * Created by LWK
 * TODO Sp存储的键值
 * 2016/8/5
 */
public class SpKeys {
    //最近登录的手机号
    public static final String LAST_LOGIN_PHONE = "lastLoginPhone";
    // 最近登录的密码
    public static final String LAST_LOGIN_PWD = "lastLoginPwd";
    //    语音消息播放是否免提
    public static final String VOICE_MSG_HANDFREE = "voiceMsgHandFree";
    //    是否开启新消息提醒
    public static final String NEW_MSG_NOTICE = "newMsgNotice";
    //    是否开启新消息铃声提醒
    public static final String NEW_MSG_NOTICE_VOICE = "newMsgNoticeVoice";
    //    是否开启新消息震动提醒
    public static final String NEW_MSG_NOTICE_VIBRATE = "newMsgNoticeVibrate";
    //    进入聊天界面时是否优先展示文字输入模式
    public static final String CHAT_TEXT_INPUT_FIRST = "chatTextInputFirst";

}
