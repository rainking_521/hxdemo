package com.rulin.huanxin_demo.service;

import android.app.Notification;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;


import com.rulin.huanxin_demo.utlis.event.ChatActEventBean;
import com.rulin.huanxin_demo.utlis.event.EventBusHelper;
import com.rulin.huanxin_demo.utlis.im.helper.HxChatHelper;
import com.rulin.huanxin_demo.utlis.im.helper.HxSdkHelper;
import com.rulin.huanxin_demo.utlis.im.listener.HxConnectListener;
import com.rulin.huanxin_demo.utlis.im.listener.HxMessageListener;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * 绑定各种环信监听的Service
 * [提高进程优先级]
 */
public class MainService extends Service {
    private static final int MAIN_SERVICE_ID = 100;
    private MainServiceBinder mBinder = new MainServiceBinder();
    private HxConnectListener mHxConnectListener;
    private HxMessageListener mHxMessageListener;
    private static final String TAG = "MainService";

    public MainService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        registHxListener();
        return mBinder;
    }

    @Override
    public void onCreate() {

        Log.i(TAG, "MainService--->OnCreate()");
        super.onCreate();
        EventBusHelper.getInstance().regist(this);
    }

    @Override
    public void onDestroy() {

        Log.i(TAG, "MainService--->onDestory()");
        super.onDestroy();
        EventBusHelper.getInstance().unregist(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.i(TAG, "MainService--->onStartCommand() ");
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) {
            //API < 18 ，此方法能有效隐藏Notification上的图标
            startForeground(MAIN_SERVICE_ID, new Notification());
        } else {
            Intent innerIntent = new Intent(this, InnerService.class);
            startService(innerIntent);
            startForeground(MAIN_SERVICE_ID, new Notification());
        }
        return START_STICKY;
    }

    @Override
    public boolean onUnbind(Intent intent) {

        Log.i(TAG, "MainService--->onUnbind()");
        unRegistHxListener();
        return super.onUnbind(intent);
    }

    /**
     * 注册环信监听
     */
    private void registHxListener() {

        Log.i(TAG, "MainService--->registHxListener()");
        mHxConnectListener = new HxConnectListener();
        HxSdkHelper.getInstance().addConnectListener(mHxConnectListener);

        mHxMessageListener = new HxMessageListener();
        HxChatHelper.getInstance().addMessageListener(mHxMessageListener);

    }

    /**
     * 解绑换新监听
     */
    private void unRegistHxListener() {

        Log.i(TAG, "MainService--->unRegistHxListener()");
        HxSdkHelper.getInstance().removeConnectListener(mHxConnectListener);
        HxChatHelper.getInstance().removeMessageListener(mHxMessageListener);

    }

    public class MainServiceBinder extends Binder {
        public MainService getService() {
            return MainService.this;
        }
    }


    public static class InnerService extends Service {

        @Nullable
        @Override
        public IBinder onBind(Intent intent) {
            return null;
        }

        @Override
        public void onCreate() {
            Log.i(TAG, "InnerService--->onCreate() ");
            super.onCreate();
        }

        @Override
        public int onStartCommand(Intent intent, int flags, int startId) {
            Log.i(TAG, "InnerService--->onStartCommand()");
            startForeground(MAIN_SERVICE_ID, new Notification());
            stopSelf();
            return super.onStartCommand(intent, flags, startId);
        }

        @Override
        public void onDestroy() {
            Log.i(TAG, "InnerService--->onDestory()");
            super.onDestroy();
        }
    }
}
