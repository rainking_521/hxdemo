package huanxin_demo.rulin.com.hxdemo.base;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import huanxin_demo.rulin.com.hxdemo.R;
import huanxin_demo.rulin.com.hxdemo.utlis.ActivityManagerUtil;
import huanxin_demo.rulin.com.hxdemo.utlis.StatusBarUtil;


/**
 * Created by Rainking on 2016/8/3.
 */
public abstract class BaseActivity extends AppCompatActivity implements View.OnClickListener {
    public ActivityManagerUtil activityManagerUtil;
    public Activity mActivity;
    private TextView tv_left;
    private TextView tv_center_title;
    private TextView tv_right;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(getLayoutResId());//把设置布局文件的操作交给继承的子类

        ViewGroup contentFrameLayout = (ViewGroup) findViewById(Window.ID_ANDROID_CONTENT);
        View parentView = contentFrameLayout.getChildAt(0);
        if (parentView != null && Build.VERSION.SDK_INT >= 14) {
            parentView.setFitsSystemWindows(true);
        }

        StatusBarUtil.transparencyBar(this);
        StatusBarUtil.setStatusBarColor(this, R.color.status_bar);


        mActivity = this;
        activityManagerUtil = ActivityManagerUtil.getInstance();
        activityManagerUtil.pushOneActivity(this);


        initView();
        initData();
        initEvents();


    }


    /**
     * 返回当前Activity布局文件的id
     *
     * @return
     */
    abstract protected int getLayoutResId();

    /**
     * 初始化视图
     */
    protected abstract void initView();

    /**
     * 初始化数据
     */
    protected abstract void initData();

    /**
     * 初始化事件
     */
    protected abstract void initEvents();


    /**
     * 通过Class跳转界面
     *
     * @param cls
     */
    protected void getStartActivity(Class<?> cls) {
        Intent intent = null;
        intent = new Intent(this, cls);
        startActivity(intent);
        overridePendingTransition(android.R.anim.fade_in,
                android.R.anim.fade_out);
    }

    protected void getFlinishActivity(Class<?> cls) {
        activityManagerUtil.finishActivity(cls);
//        overridePendingTransition(0, android.R.anim.slide_in_left);
    }

    /**
     * @param drawable_left_resId left图片
     * @param left_title          left标题
     * @param center_title        center标题
     */
    protected void getToolbarLeftCenter(int drawable_left_resId, String left_title, String center_title) {
        initToolbar(1, drawable_left_resId, 0, center_title, left_title, null);
    }

    /**
     * @param center_title center标题
     */
    protected void getToolbarCenter(String center_title) {
        initToolbar(3, 0, 0, center_title, null, null);
    }

    /**
     * @param drawable_left_resId  left图片
     * @param left_title           left标题
     * @param drawable_right_resId right图片
     * @param right_title          right标题
     * @param center_title         conter标题
     */
    protected void getToolbar(int drawable_left_resId, String left_title, int drawable_right_resId, String right_title, String center_title) {
        initToolbar(2, drawable_left_resId, drawable_right_resId, center_title, left_title, right_title);
    }

    //getToolbarCenter("center_title");
    //getToolbarLeftCenter(R.mipmap.ic_back, "left_title", "center_title");
    //getToolbar(R.mipmap.ic_back, "left_title", R.mipmap.ic_back, "right_title", "center_title");
    //getToolbar(R.mipmap.ic_back, "left_title", 0, "right_title", "center_title");
    //在父类重载onClickLeft();  onClickRight();   实现左右点击事件

    protected void initToolbar(int flag, int drawable_left_resId, int drawable_right_resId, String center_title, String left_title, String right_title) {
        tv_left = (TextView) findViewById(R.id.tv_left);
        tv_center_title = (TextView) findViewById(R.id.tv_center_title);
        tv_right = (TextView) findViewById(R.id.tv_right);
        Drawable drawable_left;
        Drawable drawable_right;
        if (drawable_left_resId != 0) {
            drawable_left = getResources().getDrawable(drawable_left_resId);
            // / 这一步必须要做,否则不会显示.
            drawable_left.setBounds(0, 0, drawable_left.getMinimumWidth(), drawable_left.getMinimumHeight());
        } else {
            drawable_left = null;
        }
        if (drawable_right_resId != 0) {
            drawable_right = getResources().getDrawable(drawable_right_resId);
            // / 这一步必须要做,否则不会显示.
            drawable_right.setBounds(0, 0, drawable_right.getMinimumWidth(), drawable_right.getMinimumHeight());
        } else {
            drawable_right = null;
        }

        switch (flag) {
            //tv_left  tv_center_title
            case 1:
                tv_right.setVisibility(View.GONE);
                tv_center_title.setVisibility(View.VISIBLE);
                tv_left.setVisibility(View.VISIBLE);
                tv_left.setText(left_title);
                tv_left.setCompoundDrawables(drawable_left, null, null, null);
                tv_left.setCompoundDrawablePadding(10);
                tv_left.setOnClickListener(this);
                tv_center_title.setText(center_title);

                break;
            //tv_left  tv_center_title  tv_right   根据需求不同给不同值
            case 2:
                tv_right.setVisibility(View.VISIBLE);
                tv_center_title.setVisibility(View.VISIBLE);
                tv_left.setVisibility(View.VISIBLE);

                tv_left.setText(left_title);
                tv_left.setCompoundDrawablePadding(10);
                tv_left.setCompoundDrawables(drawable_left, null, null, null);
                tv_right.setText(right_title);
                tv_right.setCompoundDrawablePadding(10);
                tv_right.setCompoundDrawables(null, null, drawable_right, null);
                tv_center_title.setText(center_title);
                tv_left.setOnClickListener(this);
                tv_right.setOnClickListener(this);
                break;
            //tv_center_title
            case 3:
                tv_right.setVisibility(View.GONE);
                tv_center_title.setVisibility(View.VISIBLE);
                tv_left.setVisibility(View.GONE);
                tv_center_title.setText(center_title);
                break;
            default:
                break;
        }

    }


    @Override
    protected void onDestroy() {
        //结束Activity&从栈中移除该Activity
        activityManagerUtil.popOneActivity(this);
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }


    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    @Override
    public void finish() {
        super.finish();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_left:
                onClickLeft();
                break;
            case R.id.tv_right:
                onClickRight();
                break;
        }
    }

    /**
     * tv_left点击事件
     */
    protected void onClickLeft() {
    }

    /**
     * tv_right点击事件
     */
    protected void onClickRight() {

    }

}
