package huanxin_demo.rulin.com.hxdemo.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.hyphenate.chat.EMConversation;
import com.hyphenate.chat.EMMessage;
import com.hyphenate.chat.EMTextMessageBody;

import java.util.List;

import huanxin_demo.rulin.com.hxdemo.R;

/**
 * Created by Raink on 2016/11/19.
 */

public class ItemCharListAdapter extends BaseQuickAdapter<EMConversation, BaseViewHolder> {

    public ItemCharListAdapter(List data) {
        super(R.layout.item_char_list, data);
    }


    @Override
    protected void convert(BaseViewHolder baseViewHolder, EMConversation conversation) {

//        + DateFormatUtil.stringForDate2(String.valueOf(conversation.getLastMessage().getMsgTime()));
        String messages;
        EMMessage emMessage = conversation.getLastMessage();
        switch (emMessage.getType()) {
            case TXT:
                EMTextMessageBody body = (EMTextMessageBody) emMessage.getBody();
                messages = body.getMessage();
                break;
            case IMAGE:
                messages = "IMAGE";
                break;
            case VIDEO:
                messages = "VIDEO";
                break;
            case LOCATION:
                messages = "LOCATION";
                break;
            case FILE:
                messages = "FILE";
                break;
            case CMD:
                messages = "CMD";
                break;
            case VOICE:
                messages = "VOICE";
                break;
            default:
                messages = null;
                break;
        }

        baseViewHolder.setText(R.id.tv_nickname, conversation.getUserName())
                .setText(R.id.tv_context, messages);


    }
}
