package huanxin_demo.rulin.com.hxdemo;


import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import huanxin_demo.rulin.com.hxdemo.base.BaseActivity;
import huanxin_demo.rulin.com.hxdemo.utlis.InputHelper;

public class CharDetailsActivity extends BaseActivity implements RadioGroup.OnCheckedChangeListener {

    private LinearLayout root;

    private SwipeRefreshLayout swipeLayout;
    private TextView btn_send;

    private RecyclerView rv_list;
    private RadioGroup radioGroup;
    private RadioButton rbtn_voice;
    private RadioButton rbtn_image;
    private RadioButton rbtn_minor_video;
    private RadioButton rbtn_invitation;
    private RadioButton rbtn_location;
    private FrameLayout fl_load;
    private LinearLayout footer;
    private EditText et_input_chat;

    private LinearLayout ll_input_chat;
    private int mHeight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_char_details;
    }

    @Override
    protected void initView() {
        Intent intent = getIntent();
        getToolbarLeftCenter(R.mipmap.ic_back, "返回", intent.getStringExtra("charId"));
        root = (LinearLayout) findViewById(R.id.root);
        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipeLayout);
        rv_list = (RecyclerView) findViewById(R.id.rv_list);
        footer = (LinearLayout) findViewById(R.id.footer);
        btn_send = (TextView) findViewById(R.id.btn_send);
        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        et_input_chat = (EditText) findViewById(R.id.et_input_chat);
        ll_input_chat = (LinearLayout) findViewById(R.id.ll_input_chat);
        rbtn_voice = (RadioButton) findViewById(R.id.rbtn_voice);
        rbtn_image = (RadioButton) findViewById(R.id.rbtn_image);
        rbtn_minor_video = (RadioButton) findViewById(R.id.rbtn_minor_video);
        rbtn_invitation = (RadioButton) findViewById(R.id.rbtn_invitation);
        rbtn_location = (RadioButton) findViewById(R.id.rbtn_location);
        fl_load = (FrameLayout) findViewById(R.id.fl_load);
        reset();
        onKeyboardLayout(root, footer);


    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initEvents() {
        rv_list.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                InputHelper.getInstance(getApplicationContext()).hideKeyboard(et_input_chat);
                if (fl_load.getVisibility() == View.VISIBLE) {
                    fl_load.setVisibility(View.GONE);
                    reset();
                }
                return false;
            }
        });

        radioGroup.setOnCheckedChangeListener(this);
        et_input_chat.setOnClickListener(this);
        btn_send.setOnClickListener(this);

        // 取得控件高度
        ViewTreeObserver vto2 = et_input_chat.getViewTreeObserver();
        vto2.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @Override
            public void onGlobalLayout() {
                et_input_chat.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                mHeight = et_input_chat.getHeight();

            }
        });


        et_input_chat.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // 输入过程中，还在内存里，没到屏幕上
            }

            @Override
            public void afterTextChanged(Editable s) {

                int lineCount = et_input_chat.getLineCount();//取得内容的行数

                /**
                 * 根据行数动态计算输入框的高度
                 */
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) et_input_chat
                        .getLayoutParams();

                if (lineCount <= 1) {
                    params.height = mHeight;
                    et_input_chat.setLayoutParams(params);
                } else {

                    if (lineCount == 2) {
                        params.height = mHeight * lineCount - 45;
                    } else if (lineCount == 3) {
                        params.height = mHeight * lineCount - 80;
                    } else if (lineCount == 4) {
                        params.height = mHeight * lineCount - 120;
                    } else if (lineCount == 5) {
                        params.height = mHeight * lineCount - 150;
                    } else if (lineCount == 6) {
                        params.height = mHeight * lineCount - 180;
                    }
                    et_input_chat.setLayoutParams(params);
                }


                if (et_input_chat.length() == 0) {
                    btn_send.setBackgroundResource(R.drawable.background_button_d3d4d7_send);
                } else {
                    btn_send.setBackgroundResource(R.drawable.background_button_cerulean_send);
                }

            }
        });

        et_input_chat.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // 这两个条件必须同时成立，如果仅仅用了enter判断，就会执行两次
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_DOWN) {
                    // 执行发送消息等操作

                    return true;
                }
                return false;
            }
        });
    }


    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.et_input_chat:
                if (fl_load.getVisibility() == View.VISIBLE) {
                    radioGroup.check(-1);
                    fl_load.setVisibility(View.GONE);
                    reset();
                    InputHelper.getInstance(getApplicationContext()).showKeyboard(et_input_chat);
                } else {
                    InputHelper.getInstance(getApplicationContext()).showKeyboard(et_input_chat);
                }
                break;
            case R.id.btn_send:
                et_input_chat.setText("");
                break;


        }
    }

    /**
     * 调整键盘视图
     *
     * @param root
     * @param bottomView
     */
    private void onKeyboardLayout(final View root, final View bottomView) {
        root.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect rect = new Rect();
                root.getWindowVisibleDisplayFrame(rect);
                int rootInvisibleHeight = root.getRootView().getHeight() - rect.bottom;
                if (rootInvisibleHeight > 100) {
                    int[] location = new int[2];
                    bottomView.getLocationInWindow(location);
                    int srollHeight = (location[1] + bottomView.getHeight()) - rect.bottom;
                    root.scrollTo(0, srollHeight);
                } else {
                    root.scrollTo(0, 0);
                }
            }
        });
    }

    /**
     * 重置资源图片
     */
    private void reset() {
        rbtn_voice.setButtonDrawable(R.mipmap.background_voice_nor);
        rbtn_image.setButtonDrawable(R.mipmap.background_voice_nor);
        rbtn_minor_video.setButtonDrawable(R.mipmap.background_voice_nor);
        rbtn_invitation.setButtonDrawable(R.mipmap.background_voice_nor);
        rbtn_location.setButtonDrawable(R.mipmap.background_voice_nor);
    }

    @Override
    protected void onClickLeft() {
        getFlinishActivity(CharDetailsActivity.class);
        super.onClickLeft();
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        reset();
        InputHelper.getInstance(getApplicationContext()).hideKeyboard(et_input_chat);
        if (fl_load.getVisibility() != View.VISIBLE) {
            fl_load.setVisibility(View.VISIBLE);

        }
        switch (group.getCheckedRadioButtonId()) {
            case R.id.rbtn_voice:
                rbtn_voice.setButtonDrawable(R.mipmap.background_voice_checked);


                break;
            case R.id.rbtn_image:
                rbtn_image.setButtonDrawable(R.mipmap.background_voice_checked);


                break;
            case R.id.rbtn_minor_video:
                rbtn_minor_video.setButtonDrawable(R.mipmap.background_voice_checked);

                break;
            case R.id.rbtn_invitation:
                rbtn_invitation.setButtonDrawable(R.mipmap.background_voice_checked);


                break;
            case R.id.rbtn_location:
                rbtn_location.setButtonDrawable(R.mipmap.background_voice_checked);


                break;
            default:
                break;
        }
    }
}
