package huanxin_demo.rulin.com.hxdemo;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.hyphenate.EMCallBack;
import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMConversation;
import com.hyphenate.chat.adapter.message.EMACmdMessageBody;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import huanxin_demo.rulin.com.hxdemo.adapter.ItemCharListAdapter;
import huanxin_demo.rulin.com.hxdemo.base.BaseActivity;
import huanxin_demo.rulin.com.hxdemo.utlis.SortConversationComparator;
import huanxin_demo.rulin.com.hxdemo.utlis.hxim.helper.HxChatHelper;
import huanxin_demo.rulin.com.hxdemo.utlis.hxim.helper.HxSdkHelper;

public class CharListActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {
    private static final String TAG = "CharListActivity";

    private RecyclerView mRecyclerView;
    private ItemCharListAdapter mItemCharListAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;



    private static final int PAGE_SIZE = 6;

    private int delayMillis = 1000;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_char_list;
    }

    @Override
    protected void initView() {
        getToolbarCenter("消息列表");

        mRecyclerView = (RecyclerView) findViewById(R.id.rv_list);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeLayout);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));


        initAdapter();
    }

    private void initAdapter() {


        mItemCharListAdapter = new ItemCharListAdapter(getAllConversations());

        mItemCharListAdapter.openLoadAnimation(BaseQuickAdapter.SLIDEIN_BOTTOM);
        mRecyclerView.setAdapter(mItemCharListAdapter);


        if (getAllConversations().size() == 0) {
            TextView text = new TextView(CharListActivity.this);
            text.setText("空白");
            mItemCharListAdapter.setEmptyView(text);
        }

        mRecyclerView.addOnItemTouchListener(new OnItemClickListener() {
            @Override
            public void SimpleOnItemClick(BaseQuickAdapter adapter, View view, int position) {


                Intent intent = new Intent(CharListActivity.this, CharDetailsActivity.class);
                intent.putExtra("charId", getAllConversations().get(position).getUserName());
                startActivity(intent);
                overridePendingTransition(android.R.anim.fade_in,
                        android.R.anim.fade_out);
            }
        });
    }

    @Override
    protected void initData() {

    }


    /**
     * 获取所有会话
     */
    public List<EMConversation> getAllConversations() {
        List<EMConversation> resultList = new ArrayList<>();

        //获取环信会话对象并排序
        List<EMConversation> allConversations = new ArrayList<>();
        Map<String, EMConversation> conversationMap = HxChatHelper.getInstance().getAllConversations();
        if (conversationMap != null && conversationMap.size() > 0) {
            allConversations.addAll(conversationMap.values());
            Collections.sort(allConversations, new SortConversationComparator());
        }
        //关联用户数据
        for (EMConversation conversation : allConversations) {
            resultList.add(conversation);
        }
        return resultList;
    }


    @Override
    protected void initEvents() {

    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mItemCharListAdapter.setNewData(getAllConversations());

                mSwipeRefreshLayout.setRefreshing(false);


            }
        }, delayMillis);
    }

}
