package huanxin_demo.rulin.com.hxdemo.base;

import android.app.Application;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.hyphenate.EMCallBack;
import com.hyphenate.EMError;
import com.hyphenate.chat.EMClient;

import huanxin_demo.rulin.com.hxdemo.utlis.hxim.helper.HxSdkHelper;

import static com.hyphenate.chat.a.a.a.i;


/**
 * Created by Raink on 2016/11/18.
 */

public class BaseApp extends Application {

    private static final String TAG = "BaseApp";

    private static BaseApp mInstance;

    private static Context mContext;

    // 记录环信是否已经初始化
    private boolean isInit = false;

    public static BaseApp getInstance() {
        return mInstance;
    }


    public static Context getmContext() {
        return mContext;
    }

    public static void setmContext(Context mContext) {
        BaseApp.mContext = mContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;

        // 初始化环信SDK
        HxSdkHelper.getInstance().initSdkOptions(mContext);


        EMClient.getInstance().login("wangyuyu", "wangyuyu1", new EMCallBack() {
            @Override
            public void onSuccess() {

                Log.i(TAG, "HxSdk login from server success");
                // 加载所有会话到内存
                HxSdkHelper.getInstance().loadHxLocalData(2);

            }

            @Override
            public void onError(int i, String s) {
                Log.e("HxSdkHelper", "HxSdk login fail : hxErrCode = " + i + " , msg = " + s);
                switch (i) {
                    // 网络异常 2
                    case EMError.NETWORK_ERROR:
                        Toast.makeText(BaseApp.getInstance(), "网络错误 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                        break;
                    // 无效的用户名 101
                    case EMError.INVALID_USER_NAME:
                        Toast.makeText(BaseApp.getInstance(), "无效的用户名 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                        break;
                    // 无效的密码 102
                    case EMError.INVALID_PASSWORD:
                        Toast.makeText(BaseApp.getInstance(), "无效的密码 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                        break;
                    // 用户认证失败，用户名或密码错误 202
                    case EMError.USER_AUTHENTICATION_FAILED:
                        Toast.makeText(BaseApp.getInstance(), "用户认证失败，用户名或密码错误 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                        break;
                    // 用户不存在 204
                    case EMError.USER_NOT_FOUND:
                        Toast.makeText(BaseApp.getInstance(), "用户不存在 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                        break;
                    // 无法访问到服务器 300
                    case EMError.SERVER_NOT_REACHABLE:
                        Toast.makeText(BaseApp.getInstance(), "无法访问到服务器 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                        break;
                    // 等待服务器响应超时 301
                    case EMError.SERVER_TIMEOUT:
                        Toast.makeText(BaseApp.getInstance(), "等待服务器响应超时 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                        break;
                    // 服务器繁忙 302
                    case EMError.SERVER_BUSY:
                        Toast.makeText(BaseApp.getInstance(), "服务器繁忙 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                        break;
                    // 未知 Server 异常 303 一般断网会出现这个错误
                    case EMError.SERVER_UNKNOWN_ERROR:
                        Toast.makeText(BaseApp.getInstance(), "未知的服务器异常 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                        break;
                    default:
                        Toast.makeText(BaseApp.getInstance(), "ml_sign_in_failed code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                        break;
                }

            }

            @Override
            public void onProgress(int progress, String status) {

            }
        });

    }


}
